package com.retailbilling.registration;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.retailbilling.dataentities.Biller;
import com.retailbilling.services.BillerServices;

/***
 * this class is created to make biller employee to login
 * 
 * @author BatchA
 *
 */
public class BillerRegistration {

	/***
	 * this method is for reading objects from file which returns list
	 * 
	 * @return List
	 */
	public List<Biller> readFromFile() {

		// creating a array list
		List<Biller> biller_List = null;
		biller_List = new ArrayList<>();
		// creating input stream for reading objects
		ObjectInputStream inputStream = null;
		// checking file exit if exists nothing doing ..else creating new file
		try {
			File file = new File("Biller.txt");
			if (file.exists()) {

			} else {
				file.createNewFile();
			} // reading from file
			inputStream = new ObjectInputStream(new FileInputStream("Biller.txt"));
			while (true) {
				Biller p = (Biller) inputStream.readObject();
				biller_List.add(p);// adding products into biller_List
			}
		} catch (EOFException eofException) {
			return biller_List;
		} catch (ClassNotFoundException classNotFoundException) {
			System.err.println("Object creation failed.");
		} catch (IOException ioException) {
			System.err.println("Error opening file.");
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();// closing object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
			}
		}
		return biller_List;
	}

	/***
	 * this method will display the biller objects inside a file
	 */
	public void display() {
		List<Biller> display_List = readFromFile();
		for (Biller billerReference : display_List) {
			System.out.println("enter employee id  " + billerReference.getBillerId());
			System.out.println("enter employee name  " + billerReference.getBillerName());
			System.out.println("enter employee password  " + billerReference.getBillerPassword());
			System.out.println("enter security question " + billerReference.getSecurityQuestion());
		}
	}

	/***
	 * this method is a login method which allows biller to access his
	 * functionalities after being logged in
	 * 
	 * 
	 */
	public void login() {
		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		boolean flag = false;
		System.out.println(
				"\tBILLING DEPARTMENT\n==============================================================================");

		for (int i = 0; i < 3; i++) {
			System.out.println("You have " + (3 - i) + " attempts left!!");
			// reading the biller details
			System.out.println("enter biller name");
			String billerName = sc.next();
			System.out.println("enter biller password");
			String billerPwd = sc.next();
			List<Biller> list = readFromFile();
			// checking for validity of biller
			for (Biller billerrefernce : list) {

				if ((billerrefernce.getBillerName().equals(billerName))
						&& (billerrefernce.getBillerPassword().equals(billerPwd))) {
					flag = true;
					System.out.println("Successfully logged in");
					BillerServices billerServices = new BillerServices();// creating the object for BillerServices
					billerServices.billGenerate();
					break;
				}

			}

			if (flag == false) {

				System.out.println("you have entered invalid credentials.");
			} else {
				break;
			}

		}
		// checking for forgot password
		if (flag == false) {
			System.out.println("your three attempts are completed.\n Did you forgot your password? ");
			System.out.println("\n 1. yes \n 2. no");
			int choice = 0;
			try {
				choice = sc.nextInt();
			} catch (Exception e) {
				System.err.println("\nplese enter only integer choices\n");
				return;
			}
			if (choice == 1) {
				forgotPassword();// calling forgot method
			} else {
				System.out.println("please try after sometime\n");
				System.exit(0);// exit from main menu
			}
		}

	}

	/***
	 * this method is for checking the security answer and displaying password
	 * 
	 *
	 */
	public void forgotPassword() {

		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		int count = 1;
		boolean flag = false;
		for (int i = 0; i < 3; i++) {

			System.out.println("Enter your employee id:");
			int biller_Id = 0;
			try {
				biller_Id = sc.nextInt();
			} catch (Exception e) {
				System.err.println("\n plese enter the integer value only \n");
				return;
				// TODO: handle exception
			}

			System.out.println("\nPlease answer the security question");
			System.out.println("\nWhat is your favourite dish?");
			String answer = sc.next();

			List<Biller> biller_List = readFromFile();

			// checking the security question
			for (Biller billerRefernce : biller_List) {
				// Verifying existing data with entered data
				if ((billerRefernce.getBillerId() == biller_Id)
						&& (billerRefernce.getSecurityQuestion().equals(answer))) {
					System.out.println("your password is:" + billerRefernce.getBillerPassword());
					flag = true;
					break;
				}
			}
			if (flag == false) {
				if (count < 3) {
					System.out.println("please try again");

					count++;

				} else {
					System.out.println("Too many attempts!! please try after sometime");

				}

			} else {
				break;
			}

		}

	}
}