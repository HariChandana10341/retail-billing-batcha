package com.retailbilling.registration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import com.retailbilling.dataentities.Manager;
import com.retailbilling.services.ManagerServices;

/***
 * this class is created to make manager to login
 * 
 * @author BatchA
 *
 */
public class ManagerRegistration {

	/***
	 * this method makes the manager to login and verifies his existence
	 *
	 */

	public void Registration() {

		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"\tMANAGER DEPARTMENT\n==============================================================================");
		// creating the object of manager
		Manager manager = new Manager();
		// hard coding the manager details
		manager.setManagerId(1);
		manager.setManagerName("Puran");
		manager.setManagerPassword("aspire");
		manager.setSecurityQuestion("dosa");

		// writing the object into file

		try {
			File file = new File("Manager.txt");
			file.createNewFile();
			FileOutputStream managerFile = new FileOutputStream(file);
			ObjectOutputStream obj = new ObjectOutputStream(managerFile);
			// Method for serialization of object
			obj.writeObject(manager);
			obj.close();

		} catch (Exception e) {
			return;
		}

		// checking for the validity of manager credentials
		boolean flag = false;
		for (int i = 0; i < 3; i++) {
			System.out.println("You have " + (3 - i) + " attempts left!!");
			System.out.println("enter manager name");
			String managerName = sc.next();
			System.out.println("enter manager password");
			String managerPwd = sc.next();

			if ((manager.getManagerName().equals(managerName)) && (manager.getManagerPassword().equals(managerPwd))) {
				flag = true;
				System.out.println("==============================================================================");
				System.out.println("successfully logged in");
				ManagerServices managerServices = new ManagerServices();// creating the object for ManagerServices class
				managerServices.services();
				break;
			}

			if (flag == false) {

				System.out.println("you have entered invalid credentials.");
			} else {
				break;
			}
		}

		// permission for accessing password
		if (flag == false) {
			System.out.println("your three attempts are completed.\n Did you forgot your password? ");
			System.out.println("\n 1. yes \n 2. no");
			int choice = sc.nextInt();
			if (choice == 1) {
				forgotPassword(manager);
			} else {
				System.out.println("please try after sometime");
				System.exit(0);// exit from menu
			}

		}

	}

	/***
	 * this method is for verifying the security question and displaying password
	 * 
	 * 
	 */
	public void forgotPassword(Manager manager) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		int count = 1;
		boolean flag = false;
		for (int i = 0; i < 3; i++) {

			// asking for details
			System.out.println("Enter your employee id:");
			int managerId = 0;
			try {
				managerId = sc.nextInt();
			} catch (Exception e) {
				System.err.println("\n please enter integer only\n");
				return;
				// TODO: handle exception
			}
			System.out.println("\nPlease answer the security question");
			System.out.println("\nWhat is your favourite dish?");
			String answer = sc.next();
			// creating manager object
			// verifying security question
			System.out.println(manager.getManagerId());
			System.out.println(manager.getSecurityQuestion());
			if ((manager.getManagerId() == managerId) && (manager.getSecurityQuestion().equals(answer))) {
				System.out.println("your password is:" + manager.getManagerPassword());
				flag = true;
				break;
			}

			if (flag == false) {
				if (count < 3) {
					System.out.println("please try again");

					count++;

				} else {
					System.out.println("Too many attempts!! please try after sometime");

				}

			} else {
				break;
			}

		}

	}
}
