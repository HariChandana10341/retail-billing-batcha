package com.retailbilling.registration;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.retailbilling.dataentities.Stocker;
import com.retailbilling.services.StockerServices;

/***
 * this class is created to make stocker employee to login
 * 
 * @author BatchA
 *
 */
public class StockerRegistration {

	/***
	 * this method is for reading objects from file which returns list
	 * 
	 * @return list
	 */
	public List<Stocker> readFromFile() {
		// creating a array list
		List<Stocker> stocker_List = null;
		stocker_List = new ArrayList<>();
		// creating input stream for reading objects
		ObjectInputStream inputStream = null;
		// checking file exit if exists nothing doing ..else creating new file
		try {
			File file = new File("Stocker.txt");
			if (file.exists()) {

			} else {
				file.createNewFile();
			}
			// reading from file
			inputStream = new ObjectInputStream(new FileInputStream("Stocker.txt"));
			while (true) {
				Stocker stocker = (Stocker) inputStream.readObject();
				stocker_List.add(stocker);// adding into stocker list
			}
		} catch (EOFException eofException) {
			return stocker_List;
		} catch (ClassNotFoundException classNotFoundException) {
			System.err.println("Object creation failed.");
		} catch (IOException ioException) {
			System.err.println("Error opening file.");
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();// close the object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
			}
		}
		return stocker_List;
	}

	/***
	 * this method will display the stocker objects inside a file
	 */
	public void display() {
		List<Stocker> stocker_List = readFromFile();
		for (Stocker stocketRefernce : stocker_List) {
			System.out.println("enter employee id  " + stocketRefernce.getStockerId());
			System.out.println("enter employee name  " + stocketRefernce.getStockerName());
			System.out.println("enter employee password  " + stocketRefernce.getStockerPassword());
			System.out.println("enter security question " + stocketRefernce.getSecurityQuestion());
		}
	}

	/***
	 * this method is a login method which allows stocker to access his
	 * functionalities after being logged in
	 * 
	 */
	public void login() {
		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		System.out.println(
				"\tSTOCK DEPARTMENT\n==============================================================================");

		for (int i = 0; i < 3; i++) {
			System.out.println("You have " + (3 - i) + " attempts left!!");
			// reading the stocker details
			System.out.println("enter stocker name");
			String stockerName = sc.next();
			System.out.println("enter stocker password");
			String stockerPwd = sc.next();
			System.out.println("------------------------------------------------------------------------------");

			List<Stocker> list = readFromFile();
			// checking for validity of stocker
			for (Stocker stockerReference : list) {

				if ((stockerReference.getStockerName().equals(stockerName))
						&& (stockerReference.getStockerPassword().equals(stockerPwd))) {
					flag = true;
					System.out.println("successfully logged in");
					StockerServices stockerservices = new StockerServices();
					stockerservices.Services();

					break;
				}

			}

			if (flag == false) {

				System.out.println("you have entered invalid credentials.");
			} else {
				break;
			}

		}
		// checking for forgot password
		if (flag == false) {
			System.out.println("your three attempts are completed.\n Did you forgot your password? ");
			System.out.println("\n 1. yes \n 2. no");
			int choice = 0;
			try {
				choice = sc.nextInt();
			} catch (Exception e) {
				System.err.println("enter only integer value/n");
				return;

			}
			if (choice == 1) {
				forgotPassword();// calling method
			} else {
				System.out.println("please try after sometime");
				return;
			}
		}

	}

	/***
	 * this method is for checking the security answer and displaying password
	 * 
	 */
	public void forgotPassword() {
		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		int count = 1;
		boolean flag = false;
		for (int i = 0; i < 3; i++) {

			System.out.println("Enter your employee id:");
			int stocker_Id = 0;
			try {
				stocker_Id = sc.nextInt();
			} catch (Exception e) {
				System.err.println("\n please enter integer only\n");
				return;

			}

			System.out.println("\nPlease answer the security question");
			System.out.println("\nWhat is your favourite dish?");
			String answer = sc.next();

			List<Stocker> stocker_List = readFromFile();
			// checking the security question
			for (Stocker stockerReference : stocker_List) {
				// checking existing id with given id
				if ((stockerReference.getStockerId() == stocker_Id)
						&& (stockerReference.getSecurityQuestion().equals(answer))) {
					System.out.println("your password is:" + stockerReference.getStockerPassword());
					flag = true;
					break;
				}
			}
			if (flag == false) {
				if (count < 3) {
					System.out.println("please try again");

					count++;

				} else {
					System.out.println("Too many attempts!! please try after sometime");

				}

			} else {
				break;
			}

		}

	}
}
