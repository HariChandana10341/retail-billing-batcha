package com.retailbilling.main;

import java.util.Scanner;

import com.retailbilling.registration.BillerRegistration;
import com.retailbilling.registration.ManagerRegistration;
import com.retailbilling.registration.StockerRegistration;

/**
 * This is the main class for this project
 * 
 * @author Batch-A
 *
 */
public class BillingMain {

	public static void main(String[] args) {
		// First Window
		boolean choice = true;
		// main loop

		@SuppressWarnings("resource")
		Scanner sc1 = new Scanner(System.in);// creating scanner object
		mainLoop: while (choice) {

			// department logins
			System.out.println(
					"*****************************************************************************\n1.Manager department Login \n2.Billing Department Login \n3.Stock department Login \n4.Exit\nWhich department do you want to login?");

			String operation = "";
			try {
				operation = sc1.next();
				System.out.println("==============================================================================");
			} catch (Exception e) {
				System.err.println("please enter only integer choices\n");
				return;
			}
			switch (operation)// performing user' required operations
			{
			case "1":// Manager's Login with credentials
				ManagerRegistration manager = new ManagerRegistration();

				manager.Registration();

				break;

			case "2":// Biller's Login with credentials
				BillerRegistration biller = new BillerRegistration();
				biller.login();// calling login method for biller
				break;

			case "3":// Stocker's Login with credentials
				StockerRegistration stocker = new StockerRegistration();
				stocker.login();// calling login method for biller
				break;

			case "4":// exit from menu
				System.out.println(
						"Terminated!!!!\n==============================================================================");
				break mainLoop;
			default:
				System.out.println("Enter a valid input");

			}
		}
	}

}
