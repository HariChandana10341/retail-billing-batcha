package com.retailbilling.services;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

import com.retailbilling.dataentities.*;

/**
 * This Class provides services for billing department
 * 
 * @author Batch-A
 *
 */
public class BillerServices implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * this method is used to read the data from unit file
	 * 
	 * @return list
	 */
	public List<Products> readFromFile_units() {
		// creating a array list
		List<Products> units_List = null;
		units_List = new ArrayList<>();
		// creating input stream for reading objects
		ObjectInputStream inputStream = null;
		// checking file exit if exists nothing doing ..else creating new file
		try {
			File file = new File("ProductList.txt");
			if (file.exists()) {

			} else {
				file.createNewFile();
			}

			file.setExecutable(true);
			file.setReadable(true);
			file.setWritable(true);
			// reading from file
			inputStream = new ObjectInputStream(new FileInputStream(file));
			while (true) {
				Products product = (Products) inputStream.readObject();
				units_List.add(product);//// adding products into units_list
			}
		} catch (EOFException eofException) {
			return units_List;
		} catch (ClassNotFoundException classNotFoundException) {
			System.err.println("Object creation failed.");
		} catch (IOException ioException) {
			System.err.println("Error opening file.");
			// ioException.printStackTrace();
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
			}
		}
		return units_List;
	}

	/**
	 * this method is used to read the data from weights file
	 * 
	 * @return list
	 */
	public List<Products> readFromFile_weights() {
		// creating a array list
		List<Products> weight_List = null;
		weight_List = new ArrayList<>();
		// creating input stream for reading objects
		ObjectInputStream inputStream = null;
		try {
			File file = new File("ProductList_weights.txt");
			if (file.exists()) {

			} else {
				file.createNewFile();
			}

			file.setExecutable(true);
			file.setReadable(true);
			file.setWritable(true);
			// reading from file
			inputStream = new ObjectInputStream(new FileInputStream(file));
			while (true) {
				Products product = (Products) inputStream.readObject();
				weight_List.add(product);// adding products into weights_list
			}
		} catch (EOFException eofException) {
			return weight_List;
		} catch (ClassNotFoundException classNotFoundException) {
			System.err.println("Object creation failed.");
		} catch (IOException ioException) {
			// System.err.println("Error opening file.");
			ioException.printStackTrace();
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();// close object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
			}
		}
		return weight_List;
	}

	/**
	 * This method is used to generate total bill including individual prices
	 */
	public void billGenerate() {
		// creating object for array list
		List<Products> billingList = new ArrayList<Products>();
		@SuppressWarnings("resource")
		// initializing the scanner object
		Scanner sc = new Scanner(System.in);
		double total = 0;
		boolean choice = true;
		int count = 0;
		// creating objects for stocker services class
		StockerServices services = new StockerServices();
		System.out.println("the products are");
		System.out.println("--------------------------------------------------");
		// calling the products display method
		services.displayProduct();
		//Taking invoice number
		System.out.println("Enter invoice number : ");
		String invoiceNumber="";
		try {
			invoiceNumber = sc.next();
		} catch (Exception e) {
			
		}
		
		List<Billing_details> billing_details = readFromFile_billingdetails();
		for(Billing_details billing_details2 :billing_details)
		{
			if(invoiceNumber.equalsIgnoreCase(billing_details2.getInvoiceno()))
			{
				System.out.println("Entered invoice number already exists!!");
				billGenerate();
				return;
			}
		}
		// add products into the billing lists
		while (choice) {

			System.out.println("Choose your option :\n1.Add product\t2.Generate Bill");
			int temp = 1;
			try {
				temp = Integer.parseInt(sc.next());
			} catch (Exception e) {
				System.err.println("Enter only integers");
				return;
			}
			if ((temp != 1) && (temp != 2)) {
				System.out.println("\nEnter valid choice!\n");
				continue;
			}

			if (temp == 2) {
				if (billingList.size() == 0) {
					System.out.println("You have not Entered any product in the cart!!!");
					return;
				}
				System.out.println("terminated!!!");
				break;
			}
			System.out.println("Enter the product name:");
			String pname = sc.next();
			List<Products> units_list = readFromFile_units();// storing the units file values from to units list
			List<Products> weights_list = readFromFile_weights();// storing the weights file values from to weights list
			// to check the given product in the units list
			for (Products product : units_list) {
				if (pname.equalsIgnoreCase(product.getProductname())) {
					int quantity = 0;
					count++;
					System.out.println("Enter the product quantity:");
					try {
						quantity = sc.nextInt();
						while (quantity < 0) {
							System.out.println("Enter valid quantity!");
							quantity = sc.nextInt();
						}
					} catch (Exception e) {
						System.err.println("Enter only integer values!!");
						return;
					}
					StockerServices stockservices = new StockerServices();
					boolean approval = stockservices.decreaseQuantity(quantity, pname);
					// Calculating total
					if (approval) {
						double price = quantity * (product.getPrice());
						total = total + price;
						Products billingproduct = new Products();
						// set products details
						billingproduct.setProductname(product.getProductname());
						billingproduct.setPrice(price);
						billingproduct.setQuantity(quantity);
						billingList.add(billingproduct);
					} else {
						System.out.println("\nEntered quantity is greater than product quantity");
					}

				}
			}
			// to check the given product in the weights list
			for (Products product : weights_list) {
				if (pname.equalsIgnoreCase(product.getProductname())) {
					double quantity;
					count++;
					System.out.println("Do you want to give weights in kgs or grams? \n1.Kgs\t2.Grams\n");
					int weightchoice = 0;
					try {
						weightchoice = sc.nextInt();
					} catch (Exception e) {
						System.err.println("Enter only integers!!");
						return;
					}

					if ((weightchoice != 1) && (weightchoice != 2)) {
						System.out.println("\nEnter valid choice");
						continue;
					}
					System.out.println("Enter the product quantity:");
					try {
						quantity = Double.parseDouble(sc.next());
						while (quantity < 0) {
							System.out.println("Enter valid quantity!");
							quantity = Double.parseDouble(sc.next());
						}
					} catch (Exception e) {
						System.err.println("Please enter only integer quantities!!");
						return;
					}
					if (weightchoice == 2) {
						// converting kilograms to grams
						quantity /= 1000;
					}
					StockerServices stockservices = new StockerServices();
					boolean approval = stockservices.decreaseQuantity(quantity, pname);
					// Calculating total
					if (approval) {
						double price = quantity * (product.getPrice());
						total = total + price;
						Products billingproduct = new Products();
						// set products details
						billingproduct.setProductname(product.getProductname());
						billingproduct.setPrice(price);
						billingproduct.setQuantity(quantity);
						billingList.add(billingproduct);
					} else {
						System.out.println("Entered quantity is greater than product quantity");
					}
				}
			}
			if (count == 0) {
				System.out.println("Entered product name is not found!!");
			}

		}
		// tax calculation
		double tax = ((double) 5 / 100) * total;
		total += tax;
		//creating object for date class
			Date date = new Date();
		// displaying the total bill
		System.out.println(
				"\n*****************************************************************************\n\t\tINNOMINDS STORE\n*****************************************************************************\n\tDate:"+date);
		System.out.printf("%10s %20s %15s", " Productname", "productquantity", "productprice");
		System.out.println(
				"\n========================================================================================================");

		for (Products product : billingList) {
			System.out.printf("%10s %20s %10s", product.getProductname(), product.getQuantity(), product.getPrice());
			System.out.println(
					"\n----------------------------------------------------------------------------------------------------------");
		}
		
		
		// discount calculation
		double discount = 0;
		if (total >= 500) {
			discount = ((double) 7 / 100) * total;
			total -= discount;
		}

		System.out.println("\tTax for this bill is:            " + tax);
		if (discount != 0) {
			System.out
					.println("\tYou have recieved a discount:    " + discount + "\n\tfor shopping above 500rs.");
		}
		System.out.println("\n\tTotal price is:                  " + total);
		
		customer_feedback();
		store_bill(invoiceNumber, billingList, tax, discount, total, date);
		

	}
	
	public void customer_feedback()
	{
		List<CostumerFeedback>feedbackList =readFromFile();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("\nPlease give your feedback:\n1.Very Good\n2.Good\n3.Average\n4.Poor\n5.Very poor\n");
		int feedback=0;
		try {
			feedback = sc.nextInt();
		} catch (Exception e) {
			System.err.println("Enter correct choices");
			customer_feedback();
		}
		
		Date date = new Date();
		ObjectOutputStream outputStream = null;
		
		try {
			outputStream = new ObjectOutputStream(new FileOutputStream("feedback.txt"));
		} catch (FileNotFoundException e) {
//			System.err.println("File opening error");
//			return;
			e.printStackTrace();
		} catch (IOException e) {
//			System.err.println("File opening error");
//			return;
			e.printStackTrace();
		}
		
		CostumerFeedback costumerFeedback = new CostumerFeedback();
		costumerFeedback.setFeedback(feedback);
		costumerFeedback.setDateandtime(date);
		feedbackList.add(costumerFeedback);
		
		try {
			for(CostumerFeedback costumerFeedback2:feedbackList)
			{
				outputStream.writeObject(costumerFeedback2);
			}
			
			System.out.println("Thank you for your feedback.....");
			
		} catch (IOException e) {
//			System.err.println("File opening error");
//			return;
			e.printStackTrace();
		}
	}
	
	public List<CostumerFeedback> readFromFile() {

		// creating a array list
		List<CostumerFeedback> feed_List = null;
		feed_List = new ArrayList<>();
		// creating input stream for reading objects
		ObjectInputStream inputStream = null;
		// checking file exit if exists nothing doing ..else creating new file
		try {
			File file = new File("feedback.txt");
			if (file.exists()) {

			} else {
				file.createNewFile();
			} // reading from file
			inputStream = new ObjectInputStream(new FileInputStream("feedback.txt"));
			while (true) {
				CostumerFeedback p =(CostumerFeedback)inputStream.readObject();
				feed_List.add(p);// adding products into biller_List
			}
		} catch (EOFException eofException) {
			return feed_List;
		} catch (ClassNotFoundException classNotFoundException) {
			System.err.println("Object creation failed.");
		} catch (IOException ioException) {
			System.err.println("Error opening file.");
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();// closing object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
			}
		}
		return feed_List;
	}
	
	
	public List<Billing_details> readFromFile_billingdetails() {

		// creating a array list
		List<Billing_details> billingdetails_list = null;
		billingdetails_list = new ArrayList<>();
		// creating input stream for reading objects
		ObjectInputStream inputStream = null;
		// checking file exit if exists nothing doing ..else creating new file
		try {
			File file = new File("billingdetails.txt");
			if (file.exists()) {

			} else {
				file.createNewFile();
			} // reading from file
			inputStream = new ObjectInputStream(new FileInputStream("billingdetails.txt"));
			while (true) {
				Billing_details p =(Billing_details)inputStream.readObject();
				billingdetails_list.add(p);// adding products into biller_List
			}
		} catch (EOFException eofException) {
			return billingdetails_list;
		} catch (ClassNotFoundException classNotFoundException) {
			System.err.println("Object creation failed.");
		} catch (IOException ioException) {
			System.err.println("Error opening file.");
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();// closing object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
			}
		}
		return billingdetails_list;
	}
	
	public void store_bill(String invoiceno , List<Products> billingList, double tax , double discount , double total , Date date)
	{
		//creating object and setting values for it
		Billing_details billingDetails = new Billing_details();
		billingDetails.setBilllingList(billingList);
		billingDetails.setInvoiceno(invoiceno);
		billingDetails.setDate(date);
		billingDetails.setDiscount(discount);
		billingDetails.setTax(tax);
		billingDetails.setTotal(total);
		
		List<Billing_details> billingdetails_list =(List<Billing_details>) readFromFile_billingdetails();
		
		billingdetails_list.add(billingDetails);
		ObjectOutputStream outputStream = null;
		try {
			outputStream = new ObjectOutputStream(new FileOutputStream("billingdetails.txt"));
			for(Billing_details billing_details:billingdetails_list)
			{
				outputStream.writeObject(billing_details);
			}
			
		} catch (IOException e) {
//			System.err.println("File opening error");
//			return;
			e.printStackTrace();
		}
	}
	
}