package com.retailbilling.services;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.retailbilling.dataentities.Products;

/**
 * This class used to perform some operations like add products ,delete products
 * ,update products display products
 *
 */
public class StockerServices implements Serializable {

	private static final long serialVersionUID = 1L;

	public void Services() {
		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		boolean temp = true;
		StockerServices stocker = new StockerServices();// creating object for StockerServices class
		int stockerchoice = 0;
		while (temp) {
			System.out.println(
					"*****************************************************************************\n1.Add Stock\n2.Delete stock\n3.update quantity\n4.Update price\n5.displayproducts\n6.Exit\n\r\n"
							+ "------------------------------------------------------------------------------\nWhich operation do you want to perform?");

			try {
				stockerchoice = sc.nextInt();
				System.out.println("==============================================================================");
			} catch (Exception e) {
				System.err.println("Enter only integer values");
				return;
			}

			switch (stockerchoice)// performing operations
			{
			case 1:// Add a stock
				try {
					stocker.addProduct();
				} catch (Exception e) {
					// System.err.println("Error!!");
					e.printStackTrace();
					return;
				}
				break;

			case 2:// delete stock
				try {

					stocker.deleteProduct();
				} catch (Exception e) {
					System.err.println("Error!!");
					return;
				}
				break;
			case 3:// update quantity
				stocker.updateStock();
				break;

			case 4:// update quantity
				stocker.updatePrice();
				break;

			case 5:// display the product list
				try {

					stocker.displayProduct();
				} catch (Exception e) {
					System.err.println("Error!!");
					return;
				}
				break;

			case 6:// to Exit
				temp = false;
				break;

			default:
				System.out.println("Wrong choice \n Enter valid choice");
			}
			if (stockerchoice == 6) {

				break;
			}
		}

	}

	/**
	 * //This method used for read the products from units file
	 * 
	 * @return list
	 */

	public List<Products> readFromFile_units() {
		// creating a array list
		List<Products> read_List = null;
		read_List = new ArrayList<>();
		// creating input stream for reading objects
		ObjectInputStream inputStream = null;
		// checking file exit if exists nothing doing ..else creating new file
		try {
			File file = new File("ProductList.txt");
			if (file.exists()) {

			} else {
				file.createNewFile();
			}
			// prmisisions
			file.setExecutable(true);
			file.setReadable(true);
			file.setWritable(true);
			inputStream = new ObjectInputStream(new FileInputStream(file));
			while (true) {
				Products product = (Products) inputStream.readObject();
				read_List.add(product);
			}
		} catch (EOFException eofException) {
			return read_List;
		} catch (ClassNotFoundException classNotFoundException) {
			System.err.println("Object creation failed.");
		} catch (IOException ioException) {

			ioException.printStackTrace();
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();// close object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
			}
		}
		// return the list of products
		return read_List;
	}

	/**
	 * //This method used for read the products from weights file
	 * 
	 * @return list
	 */

	public List<Products> readFromFile_weights() {
		// creating a array list
		List<Products> product_List = null;
		product_List = new ArrayList<>();
		// creating input stream for reading objects
		ObjectInputStream inputStream = null;
		// checking file exit if exists nothing doing ..else creating new file
		try {
			File file = new File("ProductList_weights.txt");
			if (file.exists()) {

			} else {
				file.createNewFile();
			}
			// permissions
			file.setExecutable(true);
			file.setReadable(true);
			file.setWritable(true);
			inputStream = new ObjectInputStream(new FileInputStream(file));
			while (true) {
				Products product = (Products) inputStream.readObject();
				product_List.add(product);
			}
		} catch (EOFException eofException) {
			return product_List;
		} catch (ClassNotFoundException classNotFoundException) {
			System.err.println("Object creation failed.");
		} catch (IOException ioException) {
			// System.err.println("Error opening file.");
			ioException.printStackTrace();
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();// close the object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
			}
		}
		// return the products of list
		return product_List;
	}

	/**
	 * This method is used to add the products to the file
	 */
	public void addProduct() {
		int flag = 0;
		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		Products product = new Products();
		List<Products> productList_weights = (List<Products>) readFromFile_weights();
		List<Products> productList_units = (List<Products>) readFromFile_units();
		// Taking values to store into the file
		System.out.println("Enter the name of the product");
		String productname = sc.next();

		List<Products> compareProducts = new ArrayList<Products>();
		compareProducts.addAll(productList_weights);
		compareProducts.addAll(productList_units);
		// Checking whether the product exists in the data
		for (int i = 0; i < compareProducts.size(); i++) {
			Products findproduct = compareProducts.get(i);
			if (findproduct.getProductname().equalsIgnoreCase(productname)) {
				System.out.println("Entered product name already exists!!");
				addProduct();
				flag = 1;

			}
		}
		if (flag == 0) {
			System.out.println("what type of quanity do u  want add\n1.weights\t2.Unit");
			int typeOfQuantity = 0;
			try {
				typeOfQuantity = sc.nextInt();
			} catch (Exception e) {
				System.err.println("plese enter integer value\n");
				return;
			}
			if (typeOfQuantity == 1) {

				try {
					product.setProductname(productname);
					System.out.println("Enter the quantity of the product");
					double quantity = Double.parseDouble(sc.next());
					while (quantity < 0) {
						System.out.println("Enter valid quantity!");
						quantity = Double.parseDouble(sc.next());
					}
					product.setQuantity(quantity);
					System.out.println("Enter the price of the product");
					double price = Double.parseDouble(sc.next());
					product.setPrice(price);
					// adding the object into the list
					productList_weights.add(product);

					FileOutputStream file = new FileOutputStream("ProductList_weights.txt");
					// Saving of object in a file
					ObjectOutputStream objectFile = new ObjectOutputStream(file);

					for (Products products_references : productList_weights) {
						// Method for serialization of object
						objectFile.writeObject(products_references);
					}
					System.out.println("Product added successfully");
					objectFile.flush();
					objectFile.close();
				} catch (FileNotFoundException e) {
					System.err.println("Error opening file");
					return;
				} catch (IOException e) {
					System.err.println("Error opening file");
					return;
				} catch (Exception e) {
					System.err.println("Error opening file");
					return;
				}
			} else if (typeOfQuantity == 2) {

				try {
					product.setProductname(productname);
					System.out.println("Enter the quantity of the product");
					int quantity_weights = Integer.parseInt(sc.next());
					product.setQuantity(quantity_weights);
					System.out.println("Enter the price of the product");
					double price = Double.parseDouble(sc.next());
					product.setPrice(price);
					// adding the object into the list
					productList_units.add(product);
					// Saving of object in a file
					FileOutputStream file = new FileOutputStream("ProductList.txt");
					ObjectOutputStream objectFile = new ObjectOutputStream(file);

					for (Products products_references : productList_units) {
						// Method for serialization of object
						objectFile.writeObject(products_references);
					}
					System.out.println("Product added successfully");
					objectFile.flush();
					objectFile.close();
				} catch (FileNotFoundException e) {
					System.err.println("Error opening file");
					return;
				} catch (IOException e) {
					System.err.println("Error opening file");
					return;
				} catch (Exception e) {
					System.err.println("Error opening file");
					return;
				}
			}

			else {
				System.out.println("wrong option");

			}

		}

	}

	/**
	 * This method is used to display the product what products are available in
	 * file
	 * 
	 * 
	 */

	public void displayProduct() {
		boolean temp = false;

		List<Products> listobject = readFromFile_units();
		// display from unitsList
		for (Products product : listobject) {
			temp = true;
			System.out.println("Product name: " + product.getProductname());
			System.out.println("Product Price : " + product.getPrice());
			int Quantity = (int) product.getQuantity();
			System.out.println("Product Quantity : " + Quantity);
			System.out.println("-----------------------------------------------");
		}
		List<Products> listobject1 = readFromFile_weights();
		// display from weightsList
		for (Products product1 : listobject1) {
			temp = true;
			System.out.println("Product name: " + product1.getProductname());
			System.out.println("Product Price : " + product1.getPrice());
			System.out.println("Product Quantity : " + product1.getQuantity());
			System.out.println("----------------------------------------------");
		}
		if (temp == false)

		{
			System.out.println("STOCK FILE IS EMNPTY.........!!!!!!\n ");
		}
	}

	/**
	 * This method is used to delete the products from the list
	 */
	public void deleteProduct() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		File units_file = new File("ProductList.txt");
		File weights_file = new File("ProductList_weights.txt");
		int count = 0;
		List<Products> units_list = readFromFile_units();
		List<Products> weights_list = readFromFile_weights();
		System.out.println("Enter the product that needs to be deleted:");
		String delete = sc.next();
		// to compare the unit list
		for (Products product : units_list) {
			if (delete.equalsIgnoreCase(product.getProductname())) {
				count++;
				units_file.delete();
				System.out.println("\nEntered product is found");
			}
		}
		// to compare the weights list
		for (Products product : weights_list) {
			if (delete.equalsIgnoreCase(product.getProductname())) {
				count++;
				weights_file.delete();
				System.out.println("\nEntered product is found");
			}
		}
		if (count == 0) {
			System.out.println("Entered product name is not found!!");
			// to add the remaining products to file after deleting the products
		} else {
			try {
				units_file.createNewFile();
				weights_file.createNewFile();
				ObjectOutputStream units_out = new ObjectOutputStream(new FileOutputStream(units_file));
				ObjectOutputStream weights_out = new ObjectOutputStream(new FileOutputStream(weights_file));
				for (Products product : units_list) {
					if (delete.equalsIgnoreCase(product.getProductname())) {
						continue;
					} else {
						units_out.writeObject(product);
					}
				}
				for (Products product : weights_list) {
					if (delete.equalsIgnoreCase(product.getProductname())) {
						continue;
					} else {
						weights_out.writeObject(product);
					}
				}
				System.out.println("\nItem is deleted\n");
				// closing the objects
				units_out.flush();
				units_out.close();
				weights_out.flush();
				weights_out.close();
			} catch (IOException e) {
				System.err.println("Error opening file");
				return;
			}
		}
	}

	/**
	 * This method is used to update the stock quantity
	 */
	public void updateStock() {
		@SuppressWarnings("resource")
		// Initializing the scanner
		Scanner sc = new Scanner(System.in);
		System.out.println("which operation do u want to perform\n1.increasequanity\t2.decrease Quantity\n");
		int operation = Integer.parseInt(sc.next());
		// file creation for units and weights files
		File units_file = new File("ProductList.txt");
		File weights_file = new File("ProductList_weights.txt");
		int count = 0;
		// lists creation for units and weights files
		List<Products> units_list = readFromFile_units();// reading products from units file and storing into units list
		List<Products> weights_list = readFromFile_weights();// reading products from weights_file and storing into
																// weights_list
		System.out.println("Enter the product name whose quantity needs to be updated:");
		String updateproduct = sc.next();
		// verifying whether the product exists in units file
		for (Products product : units_list) {
			if (updateproduct.equalsIgnoreCase(product.getProductname())) {
				count++;
				units_file.delete();
				System.out.println("\nEntered product is found\n");
			}
		}
		// verifying whether the product exists in weights file
		for (Products product : weights_list) {
			if (updateproduct.equalsIgnoreCase(product.getProductname())) {
				count++;
				weights_file.delete();
				System.out.println("\nEntered product is found\n");
			}
		}

		if (count == 0) {
			System.out.println("Entered product name is not found!!");
		} else {

			try {
				boolean flag = false;
				System.out.println("Enter the product quantity:");
				double quantity = Double.parseDouble(sc.next());
				// units file comparing given product quantity present or not
				units_file.createNewFile();
				ObjectOutputStream units_out = new ObjectOutputStream(new FileOutputStream(units_file));
				ObjectOutputStream weights_out = new ObjectOutputStream(new FileOutputStream(weights_file));
				for (Products product : units_list) {
					if (updateproduct.equalsIgnoreCase(product.getProductname())) {
						if (product.getQuantity() < quantity && operation == 2) {

							System.out.println("Entered Quantity is greater than the existing quantity!!");
							units_out.writeObject(product);
							break;
						} else {
							if (operation == 2) {
								quantity = quantity * (-1);
							}

							quantity += product.getQuantity();
							product.setQuantity(quantity);
							flag = true;
						}
					}
					units_out.writeObject(product);
				}
				// weights file comparing given product quantity present or not
				for (Products product : weights_list) {
					if (updateproduct.equalsIgnoreCase(product.getProductname())) {
						if ((product.getQuantity() < quantity) && (operation == 2)) {
							System.out.println("Entered Quantity is greater than the existing quantity!!");
							// Method for serialization of object
							weights_out.writeObject(product);
							break;
						} else {
							if (operation == 2) {
								quantity = quantity * (-1);
							}
							quantity += product.getQuantity();
							product.setQuantity(quantity);
							flag = true;
						}

					}
					// Method for serialization of object
					weights_out.writeObject(product);
				}
				if (flag) {
					System.out.println("\nItem is updated\n");
				}
				// closing the object
				units_out.close();
				weights_out.close();
			} catch (IOException e) {
				System.err.println("Error opening file");
				return;
			} catch (Exception e) {
				System.err.println("Error opening file");
				return;
			}
		}

	}

	/**
	 * This method is used to update the stock price
	 */
	public void updatePrice() {
		@SuppressWarnings("resource")
		// Initializing the scanner
		Scanner sc = new Scanner(System.in);
		System.out.println("which operation do u want to perform\n1.increase price\t2.decrease price\n");
		int operation = Integer.parseInt(sc.next());
		// file creation for units and weights files
		File units_file = new File("ProductList.txt");
		File weights_file = new File("ProductList_weights.txt");
		int count = 0;
		// lists creation for units and weights files
		List<Products> units_list = readFromFile_units();// reading products from units file and storing into units list
		List<Products> weights_list = readFromFile_weights();// reading products from weights_file and storing into
																// weights_list
		System.out.println("Enter the product name whose price needs to be updated:");
		String updateproduct = sc.next();
		// verifying whether the product exists in units file
		for (Products product : units_list) {
			if (updateproduct.equalsIgnoreCase(product.getProductname())) {
				count++;
				units_file.delete();// deleting from file
				System.out.println("\nEntered product is found\n");
			}
		}
		// verifying whether the product exists in weights file
		for (Products product : weights_list) {
			if (updateproduct.equalsIgnoreCase(product.getProductname())) {
				count++;
				weights_file.delete();// deleting from file
				System.out.println("\nEntered product is found\n");
			}
		}

		if (count == 0) {
			System.out.println("Entered product name is not found!!");
		} else {

			try {
				boolean flag = false;
				System.out.println("Enter the new product Price:");
				double price = Double.parseDouble(sc.next());
				// units file comparing given product quantity present or not
				units_file.createNewFile();
				ObjectOutputStream units_out = new ObjectOutputStream(new FileOutputStream(units_file));
				ObjectOutputStream weights_out = new ObjectOutputStream(new FileOutputStream(weights_file));
				for (Products product : units_list) {
					if (updateproduct.equalsIgnoreCase(product.getProductname())) {
						if (product.getPrice() < price && operation == 2) {

							System.out.println("Entered price is greater than the existing price!!");
							units_out.writeObject(product);
							break;
						} else {
							if (operation == 2) {
								price = price * (-1);
							}

							price += product.getPrice();
							product.setPrice(price);
							flag = true;
						}
					}
					// Method for serialization of object
					units_out.writeObject(product);
				}
				// weights file comparing given product quantity present or not
				for (Products product : weights_list) {
					if (updateproduct.equalsIgnoreCase(product.getProductname())) {
						if ((product.getPrice() < price) && (operation == 2)) {
							System.out.println("Entered price is greater than the existing price!!");
							// Method for serialization of object
							weights_out.writeObject(product);
							break;
						} else {
							if (operation == 2) {
								price = price * (-1);
							}
							price += product.getPrice();
							product.setPrice(price);
							flag = true;
						}

					}
					// Method for serialization of object
					weights_out.writeObject(product);
				}
				if (flag) {
					System.out.println("\nItem is updated\n");
				}
				// closing the objects
				units_out.close();
				weights_out.close();
			} catch (IOException e) {
				System.err.println("Error opening file");
				return;
			} catch (Exception e) {
				System.err.println("Error opening file");
				return;
			}
		}

	}

	/**
	 * this method is for decreasing the quantity when calculating price
	 * 
	 *
	 * @return Boolean
	 */
	public boolean decreaseQuantity(double quantity, String productName) {
		File file_units = new File("ProductList.txt");
		File file_weights = new File("ProductList_weights.txt");
		int count_units = 0;
		int count_weights = 0;
		// reading the file into list
		List<Products> listobject_units = readFromFile_units();// reading products from units file and storing into
																// units list
		List<Products> listobject_weights = readFromFile_weights();// reading products from weights file and storing
																	// into weights list

		// verifying whether the product exists in units file
		for (Products product : listobject_units) {
			if (productName.equalsIgnoreCase(product.getProductname())) {
				count_units++;
				file_units.delete();// deleting from file
			}
		}
		// verifying whether the product exists in weights file
		for (Products product : listobject_weights) {
			if (productName.equalsIgnoreCase(product.getProductname())) {
				count_weights++;
				file_weights.delete();// deleting from file
			}
		}

		if (count_units == 0 && count_weights == 0) {
			System.out.println("Entered product name is not found!!");
			return false;
		} else {
			try {
				if (count_units == 1) {
					file_units.createNewFile();
					@SuppressWarnings("resource")
					ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file_units));
					for (Products product : listobject_units) {
						if (productName.equalsIgnoreCase(product.getProductname())) {
							quantity = product.getQuantity() - quantity;
							if (quantity <= 0) {
								// Method for serialization of object
								out.writeObject(product);

								return false;
							}
							product.setQuantity(quantity);
							out.writeObject(product);
						} else {
							out.writeObject(product);
						}
					}
					out.close();
					return true;
				}

				if (count_weights == 1) {
					file_weights.createNewFile();
					@SuppressWarnings("resource")
					ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file_weights));
					for (Products product : listobject_weights) {
						if (productName.equalsIgnoreCase(product.getProductname())) {
							quantity = product.getQuantity() - quantity;
							if (quantity <= 0) {
								out.writeObject(product);// Method for serialization of object
								return false;
							}
							product.setQuantity(quantity);
							out.writeObject(product);
						} else {
							out.writeObject(product);
						}
					}
					out.close();
					return true;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.err.println("Error opening file");
				return false;
			}
		}
		return false;
	}
}