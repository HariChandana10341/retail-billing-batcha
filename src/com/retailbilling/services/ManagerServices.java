package com.retailbilling.services;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import com.retailbilling.dataentities.Biller;
import com.retailbilling.dataentities.Billing_details;
import com.retailbilling.dataentities.CostumerFeedback;
import com.retailbilling.dataentities.Products;
import com.retailbilling.dataentities.Stocker;
import com.retailbilling.registration.BillerRegistration;
import com.retailbilling.registration.StockerRegistration;

/***
 * this class is created for accessing manager services
 * 
 * @author BatchA
 *
 */
public class ManagerServices {

	/***
	 * this method is for selecting manager choice for accessing his services
	 * 
	 * 
	 */
	@SuppressWarnings("resource")
	public void services() {

		boolean temp = true;
		int managerchoice = 0;

		ManagerServices manager = new ManagerServices();// creating objects for ManagerServices class
		while (temp) {
			System.out.println(
					"*****************************************************************************\n1.Add a Stocker\n2.Add a biller\n3.Delete a biller\n4.Delete a stocker\n5.Display Employees\n6.Display Products\n7.View feedback of store\n8.View previous bills\n9.Exit\n==============================================================================\nWhich operation do you want to perform?");

			Scanner sc = new Scanner(System.in);
			managerchoice = 0;
			try {
				managerchoice = sc.nextInt();
				System.out.println("==============================================================================");
			} catch (Exception e) {
				System.err.println("plese enter integer value only\n");
				return;

			}
			// manager's operation choices
			switch (managerchoice)// performing operations
			{
			case 1:// Add a stocker
				try {
					manager.addStocker();
				} catch (Exception e) {
					System.err.println("Error!!");
					return;
				}
				break;

			case 2:// Add a biller
				try {
					manager.addBiller();
				} catch (Exception e) {
					System.err.println("Error!!");
					return;
				}
				break;

			case 3:// Delete a biller
				try {
					manager.deleteBiller();
				} catch (Exception e) {
					System.err.println("There is no data of employees\tContact Manager");
					return;
				}
				break;

			case 4:// Delete a stocker
				try {
					manager.deleteStocker();
				} catch (Exception e) {
					System.err.println("There is no data of employees\tContact Manager");
					return;
				}
				break;
			case 5:// Display employees
				try {
					manager.displayEmployees();
				} catch (Exception e) {
					System.err.println("There is no data of employees\tContact Manager");
					return;
				}
				break;

			case 6:
				StockerServices stockerServices = new StockerServices();
				System.out.println("The products are :");
				stockerServices.displayProduct();
				break;

			case 7:
				displayFeedback();
				break;
				
			case 8 :
				displayBillDetails();
				break;
				
			case 9:// exit from menu
				temp = false;

				break;

			default:
				System.out.println("Wrong choice \n Enter valid choice");
			}
			// exiting the entire loop
			if (managerchoice == 9) {
				break;
			}

		}
	}

	/***
	 * this method is for adding stocker details into file
	 * 
	 * 
	 */
	public void addStocker() {

		StockerRegistration stocker = new StockerRegistration();// creating objects for StockerRegistration class

		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		// creating a list and storing the objects which we read from the file
		List<Stocker> stocker_list = stocker.readFromFile();
		ObjectOutputStream outStream = null;
		// creating object for Stocker Class
		Stocker stocker_add = new Stocker();
		System.out.println("enter stocker id");
		int stockerId = 0;
		try {
			stockerId = sc.nextInt();
		} catch (Exception e) {
			System.err.println("plese enter the integer only");
			return;
		}

		stocker_add.setStockerId(stockerId);
		try {
			outStream = new ObjectOutputStream(new FileOutputStream("Stocker.txt"));
			// checking whether the id already exists or not
			for (Stocker p : stocker_list) {
				while (p.getStockerId() == stockerId) {
					System.out.println("employee with this id is already exist");
					System.out.println("enter stocker id");

					stockerId = sc.nextInt();
				}
			}
			stocker_add.setStockerId(stockerId);

		} catch (IOException ioException) {
			System.err.println("Error opening file.");
			return;
		} catch (NoSuchElementException noSuchElementException) {
			System.err.println("Invalid input.");
			return;
		} finally {
			try {
				if (outStream != null)
					outStream.close();
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
				return;
			}
		}
		// reading the variables
		System.out.println("enter stocker name");
		String stockername = sc.next();
		stocker_add.setStockerName(stockername);
		System.out.println("enter stocker password");
		String stockerpassword = sc.next();
		stocker_add.setStockerPassword(stockerpassword);
		System.out.println("What is your favorite dish");
		String securityQuestion = sc.next();
		stocker_add.setSecurityQuestion(securityQuestion);
		stocker_list.add(stocker_add);
		
		// System.out.println(list.toString());
		try {
			int temp = 0;
			// Saving of object in a file
			outStream = new ObjectOutputStream(new FileOutputStream("Stocker.txt"));
			if (stocker_list.size() > 0) {
				for (Stocker p : stocker_list) {

					
					// Method for serialization of object
					outStream.writeObject(p);
					temp=1;
				}
			}
			if (temp==1) {
				System.out.println("Stocker Added Sucessfully....");
				System.out.println("----------------------------\n");
			}
			

		} catch (IOException ioException) {
			System.err.println("Error opening file.");
			return;
		} catch (NoSuchElementException noSuchElementException) {
			System.err.println("Invalid input.");
			return;
		} finally {
			try {
				if (outStream != null)
					outStream.close();
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
				return;
			}
		}
	}

	/***
	 * this method is for deleting stocker details from the existing file
	 * 
	 * 
	 */
	public void deleteStocker() {
		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		boolean flag = false;
		System.out.println("enter the employee id");
		int empId = 0;
		try {
			empId = sc.nextInt();
		} catch (Exception e) {
			System.err.println("plese enter integer value only\n");
			return;

		}
		StockerRegistration stocker = new StockerRegistration();// creating object for StockerRegistration class
		List<Stocker> stocker_List = stocker.readFromFile();
		ObjectOutputStream outStream = null;
		// checking the existence of stocker inside a file and deleting
		try {

			outStream = new ObjectOutputStream(new FileOutputStream("Stocker.txt"));
			for (Stocker stocker_refernce : stocker_List) {

				if (stocker_refernce.getStockerId() == empId) {
					flag = true;
				} else {
					outStream.writeObject(stocker_refernce);
				}
			}
			if (flag == false) {
				System.out.println("emp with such ID does not exist");
				deleteStocker();

			} else {
				System.out.println("Employee removed sucessfully......");
				System.out.println("----------------------------------\n");
			}

		} catch (IOException ioException) {
			System.err.println("Error opening file.");
			return;
		} catch (NoSuchElementException noSuchElementException) {
			System.err.println("Invalid input.");
			return;
		} finally {
			try {
				if (outStream != null)
					outStream.close();// closing the object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
				return;
			}
		}

	}

	/***
	 * this method is for adding biller details into file
	 * 
	 * 
	 */
	public void addBiller() {

		BillerRegistration biller = new BillerRegistration();// creating the objest BillerRegistration for class

		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		// creating a list and storing the objects which we read from the file
		List<Biller> biller_list = biller.readFromFile();
		// Saving of object in a file
		ObjectOutputStream outStream = null;
		Biller add_Biller = new Biller();// creating the object for biller
		System.out.println("enter biller id");

		int billerId = 0;
		try {
			billerId = sc.nextInt();
		} catch (Exception e) {
			System.err.println("\n please enter integer only\n");
			return;
		}
		add_Biller.setBillerId(billerId);
		try {
			outStream = new ObjectOutputStream(new FileOutputStream("Biller.txt"));
			// checking whether the id already exists or not
			for (Biller biller_refernce : biller_list) {
				while (biller_refernce.getBillerId() == billerId) {
					System.out.println("employee with this id is already exist");
					System.out.println("enter biller id");

					billerId = sc.nextInt();
				}
			}
			add_Biller.setBillerId(billerId);

		} catch (IOException ioException) {
			System.err.println("Error opening file.");
			return;
		} catch (NoSuchElementException noSuchElementException) {
			System.err.println("Invalid input.");
			return;
		} catch (Exception e) {
			System.err.println("\n please enter integer only\n");

		} finally {
			try {
				if (outStream != null)
					outStream.close();// closing the object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
				return;
			}
		}
		// reading the variables
		System.out.println("enter biller name");
		String billername = sc.next();
		add_Biller.setBillerName(billername);
		System.out.println("enter biller password");
		String billerpassword = sc.next();
		add_Biller.setBillerPassword(billerpassword);
		System.out.println("What is your favorite dish");
		String securityQuestion = sc.next();
		add_Biller.setSecurityQuestion(securityQuestion);
		biller_list.add(add_Biller);

		try {
			boolean temp = false;

			// Saving of object in a file
			outStream = new ObjectOutputStream(new FileOutputStream("Biller.txt"));
			if (biller_list.size() > 0) {
				for (Biller biller_Refernce : biller_list) {

					outStream.writeObject(biller_Refernce);
					temp = true;
				}
			}
			if (temp) {
				System.out.println("Biller Added Sucessfully....");
				System.out.println("------------------------------------\n");
			}

		} catch (IOException ioException) {
			System.err.println("Error opening file.");
			return;
		} catch (NoSuchElementException noSuchElementException) {
			System.err.println("Invalid input.");
			return;
		} finally {
			try {
				if (outStream != null)
					outStream.close();// closing the object
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
				return;
			}
		}

	}

	/***
	 * this method is for deleting biller details from the existing file
	 * 
	 * 
	 */
	public void deleteBiller() {
		// Initializing the scanner
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		System.out.println("enter the employee id");
		int biller_Id = 0;
		try {
			biller_Id = sc.nextInt();
		} catch (Exception e) {
			System.err.println("please enter integer value\n");
			return;
			// TODO: handle exception
		}
		BillerRegistration biller = new BillerRegistration();// creating object for BillerRegistration class
		List<Biller> biller_List = biller.readFromFile();
		ObjectOutputStream outStream = null;
		// checking for the existence of biller inside a file and deleting
		try {

			outStream = new ObjectOutputStream(new FileOutputStream("Biller.txt"));
			for (Biller biller_Refernces : biller_List) {

				if (biller_Refernces.getBillerId() == biller_Id) {
					flag = true;
				} else {
					outStream.writeObject(biller_Refernces);
				}
			}
			if (flag == false) {
				System.out.println("emp with such ID does not exist");
				deleteBiller();

			} else {
				System.out.println("Employeee Account Deleted Sucessfully.....");
				System.out.println("------------------------------------------------\n");
			}
		} catch (IOException ioException) {
			System.err.println("Error opening file.");
			return;
		} catch (NoSuchElementException noSuchElementException) {
			System.err.println("Invalid input.");
			return;
		} finally {
			try {
				if (outStream != null)
					outStream.close();
			} catch (IOException ioException) {
				System.err.println("Error closing file.");
				return;
			}
		}

	}

	public void displayEmployees() {
		BillerRegistration biller = new BillerRegistration(); // creating object for BillerRegistration class
		List<Biller> billerList = biller.readFromFile();
		StockerRegistration stocker = new StockerRegistration();// creating object for StockerRegistration class
		List<Stocker> stockerList = stocker.readFromFile();
		boolean temp = false;
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the choice\n1.BillerEmployees\t2.StockerEmployees\n");
		int choice = scanner.nextInt();

		if (choice == 1) {

			for (Biller biller1 : billerList) {
				temp = true;
				System.out.println("Biller name: " + biller1.getBillerName());
				System.out.println("Biller id : " + biller1.getBillerId());
				System.out.println("-----------------------------------------------\n");
			}
			if (temp == false) {

				System.out.println("Biller FILE IS EMNPTY.........!!!!!!\n ");
			}
		} else if (choice == 2) {

			for (Stocker stocker1 : stockerList) {
				temp = true;
				System.out.println("stocker name: " + stocker1.getStockerName());
				System.out.println("stocker id: " + stocker1.getStockerId());
				System.out.println("----------------------------------------------\n");
			}
			if (temp == false)

			{
				System.out.println("Stocker FILE IS EMNPTY.........!!!!!!\n ");
			}
		} else {
			System.out.println("not a valid choice\n");
		}

	}
	
	public void displayFeedback()
	{
		List<CostumerFeedback> feedbackList = new ArrayList<CostumerFeedback>();
		
		File file = new File("feedback.txt");
		if(file.exists())
		{
			
		}
		else
		{
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		ObjectInputStream inputStream = null;
		try {
			inputStream = new ObjectInputStream(new FileInputStream("feedback.txt"));
			while (true) {
				CostumerFeedback feedback = (CostumerFeedback) inputStream.readObject();
				feedbackList.add(feedback);// adding products into biller_List
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		String feedback="";
		System.out.println("The feedback of the store is:\n");
		for(CostumerFeedback cosfeedback : feedbackList)
		{
			switch(cosfeedback.getFeedback())
			{
			case 1 : feedback="Very Good";break;
			
			case 2 : feedback="Good";break;
			
			case 3 : feedback="Average";break;
			
			case 4 : feedback="Poor";break;
			
			case 5 : feedback="Very Poor";break;
				
			}
			System.out.println("Date: "+cosfeedback.getDateandtime()+"\tFeedback : "+feedback+"\n");
		}
	}
	
	public void displayBillDetails()
	{
		Scanner sc = new Scanner(System.in);
		
		BillerServices billerServices = new BillerServices();
		List<Billing_details> billing_details = billerServices.readFromFile_billingdetails();
		
		System.out.println("The invoice numbers present are :");
		
		for(Billing_details billing_details2 : billing_details)
		{
			System.out.println(billing_details2.getInvoiceno());
		}
		
		System.out.println("Enter The invoice number:");
		String invoiceNo = sc.next();
		int count = 0;
		for(Billing_details billing_details3 : billing_details)
		{
			if(billing_details3.getInvoiceno().equalsIgnoreCase(invoiceNo))
			{
				List<Products> productslist = billing_details3.getBilllingList();
				
				System.out.println(
						"\n*****************************************************************************\n\t\tINNOMINDS STORE\n*****************************************************************************\n\tDate:"+billing_details3.getDate());
				System.out.printf("%10s %20s %15s", " Productname", "productquantity", "productprice");
				System.out.println(
						"\n========================================================================================================");

				for (Products product : productslist) {
					System.out.printf("%10s %20s %10s", product.getProductname(), product.getQuantity(), product.getPrice());
					System.out.println(
							"\n----------------------------------------------------------------------------------------------------------");
				}
				
				System.out.println("\tTax for this bill is:            " + billing_details3.getTax());
				if (billing_details3.getDiscount() != 0) {
					System.out
							.println("\tYou have recieved a discount:    " + billing_details3.getDiscount() + "\n\tfor shopping above 500rs.");
				}
				System.out.println("\n\tTotal price is:                  " + billing_details3.getTotal());
				count++;
			}
		}
		if(count == 0)
		{
			System.out.println("Entered invoice number doesn't exist!!");
		}
	}

}
