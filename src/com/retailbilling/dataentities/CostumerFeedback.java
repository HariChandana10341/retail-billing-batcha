package com.retailbilling.dataentities;
/**
 * This class is created to store customer feedbacks
 * @author Batch-A
 *
 */

import java.io.Serializable;
import java.util.Date;

public class CostumerFeedback implements Serializable{
	
	private int feedback;
	private Date dateandtime;
	
	public int getFeedback() {
		return feedback;
	}
	public void setFeedback(int feedback) {
		this.feedback = feedback;
	}
	public Date getDateandtime() {
		return dateandtime;
	}
	public void setDateandtime(Date dateandtime) {
		this.dateandtime = dateandtime;
	}
	
	
}
