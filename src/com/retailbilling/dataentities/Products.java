package com.retailbilling.dataentities;

import java.io.Serializable;

/**
 * This class contains all parameters of product list like
 * product name,price,quantity Also have the add product method ,and display
 * method , delete product
 */

public class Products implements Serializable {

	private static final long serialVersionUID = 1L;
	private String productname;
	private double price;
	private double quantity;

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Products [productname=" + productname + ", price=" + price + ", quantity=" + quantity
				+ ", getProductname()=" + getProductname() + ", getPrice()=" + getPrice() + ", getQuantity()="
				+ getQuantity() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
