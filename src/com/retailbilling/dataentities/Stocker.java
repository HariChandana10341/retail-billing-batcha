package com.retailbilling.dataentities;

import java.io.Serializable;

/***
 * creating object for stocker and creating getters and setters for all his
 * credentials
 * 
 * @author BatchA
 *
 */
public class Stocker implements Serializable {

	private static final long serialVersionUID = 1L;
	// initializing the stocker variables
	private int stockerId;
	private String stockerName;
	private String stockerPassword;
	private String securityQuestion;

	// creating setters and getters for stocker variables
	public int getStockerId() {
		return stockerId;
	}

	public void setStockerId(int stockerId) {
		this.stockerId = stockerId;
	}

	public String getStockerName() {
		return stockerName;
	}

	public void setStockerName(String stockerName) {
		this.stockerName = stockerName;
	}

	public String getStockerPassword() {
		return stockerPassword;
	}

	public void setStockerPassword(String stockerPassword) {
		this.stockerPassword = stockerPassword;
	}

	public String getSecurityQuestion() {
		return securityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	@Override
	public String toString() {
		return new StringBuffer().append(this.stockerId)

				.append(" ").append(this.stockerName).append("  ").append(this.stockerPassword).append(" ")
				.append(this.securityQuestion).append("\n").toString();

	}
}
