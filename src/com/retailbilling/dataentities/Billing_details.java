package com.retailbilling.dataentities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Billing_details implements Serializable{
	
	private String invoiceno;
	private List billlingList;
	private Date date;
	private double tax;
	private double discount;
	private double total;
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getInvoiceno() {
		return invoiceno;
	}
	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}
	public List getBilllingList() {
		return billlingList;
	}
	public void setBilllingList(List billlingList) {
		this.billlingList = billlingList;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
