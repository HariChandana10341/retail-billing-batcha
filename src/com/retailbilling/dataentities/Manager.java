

package com.retailbilling.dataentities;

import java.io.Serializable;

/***
 * creating manager object and creating getters and setter methods manager
 * credentials
 * 
 * @author BatchA
 *
 */
public class Manager implements Serializable {

	private static final long serialVersionUID = 1L;
	// initializing the manager variables as private
	private int managerId;
	private String managerName;
	private String managerPassword;
	private String securityQuestion;

	// creating the getter and setter methods for each credential
	public int getManagerId() {
		return managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getManagerPassword() {
		return managerPassword;
	}

	public void setManagerPassword(String managerPassword) {
		this.managerPassword = managerPassword;
	}

	public String getSecurityQuestion() {
		return securityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	@Override
	public String toString() {
		return new StringBuffer().append(this.managerId)

				.append(" ").append(this.managerName).append("  ").append(this.managerPassword).append(" ")
				.append(this.securityQuestion).append("\n").toString();

	}
}
