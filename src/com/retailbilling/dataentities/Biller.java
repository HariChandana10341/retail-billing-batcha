package com.retailbilling.dataentities;

import java.io.Serializable;

/***
 * creating object for biller employee which contains getters and setters
 * 
 * @author BatchA
 *
 */
public class Biller implements Serializable {

	private static final long serialVersionUID = 1L;
	// initializing the variables as private
	private int billerId;
	private String billerName;
	private String billerPassword;
	private String securityQuestion;

	// creating getters and setters for biller credentials
	public int getBillerId() {
		return billerId;
	}

	public void setBillerId(int billerId) {
		this.billerId = billerId;
	}

	public String getBillerName() {
		return billerName;
	}

	public void setBillerName(String billerName) {
		this.billerName = billerName;
	}

	public String getBillerPassword() {
		return billerPassword;
	}

	public void setBillerPassword(String billerPassword) {
		this.billerPassword = billerPassword;
	}

	public String getSecurityQuestion() {
		return securityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	@Override
	public String toString() {
		return new StringBuffer().append(this.billerId)

				.append(" ").append(this.billerName).append("  ").append(this.billerPassword).append(" ")
				.append(this.securityQuestion).append("\n").toString();

	}
}
