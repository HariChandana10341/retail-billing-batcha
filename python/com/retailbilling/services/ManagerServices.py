import pickle;


from com.retailbilling.dataentities.Stocker import Stocker
from com.retailbilling.dataentities.Biller import Biller
from com.retailbilling.registration.StockerRegistration import StockerRegistration
from com.retailbilling.registration.BillerRegistration import BillerRegistration

class ManagerServices:
    
    stockerList = []
    billerList = []
    def services(self):
        flag = True
        while(flag):
            choice=input("Choose an option : \n1.Add stocker \n2.Display stockers \n3. Add biller \n4.Display Biller \n5.delete stocker \n6.delete biller")
            if(choice=="1") :
                self.addStocker()
            elif(choice=="2") :
                self.displayStockers()
            elif(choice=="3") :
                self.addBiller()
            elif(choice=="4") :
                self.displayBillers()
            elif(choice=="5") :
                self.delete_stocker()
            elif(choice=="6") :
                self.delete_biller()
            elif(choice == "7"):
                break
            else :
                print("Enter correct choice")    

    
    
    def addStocker(self):
        stockerRegistration = StockerRegistration()
        self.stockerList = stockerRegistration.read_from_file_stocker()
        stocker = Stocker();
        print("Enter stocker ID")
        stockerId = input()
        print(len(self.stockerList))
        for  stockercheck in self.stockerList:
           
            if(stockercheck.getStockerId() == stockerId):
                print("stocker id is already exists\n")
                self.addStocker() 
                return        
        stocker.setStockerId(stockerId)
        print("Enter stocker name")
        stockerName = input()
        stocker.setStockerName(stockerName)
        print("Enter stocker password")
        stockerPassword = input()
        stocker.setStockerPassword(stockerPassword)
        print("what is your favorite dish")
        securityQuestion = input()
        stocker.setSecurityQuestion(securityQuestion)
    
    
        self.stockerList.append(stocker)
        print("Stocker added sucesfully...\n")
        with open("stocker.txt","wb") as f:
            pickle.dump(self.stockerList,f)
            
    def delete_stocker(self):
        stockerRegistration = StockerRegistration()
        self.stockerList = stockerRegistration.read_from_file_stocker()
        id=input("eneter stocker id")
        count=0
        
        for i in range(0,len(self.stockerList)):
            stocker=self.stockerList[i]
            if(stocker.getStockerId()==id):
                    count=count+1
                    self.stockerList.remove(stocker)
                    break;
        with open("stocker.txt","wb") as f:
            pickle.dump(self.stockerList,f)
                    
        if(count==0):
            print("stocker id is  not found\n") 
        else:
            print("stocker deleted sucesfully\n") 


        
    
    def displayStockers(self):
        stockerRegistration = StockerRegistration()
        self.stockerList = stockerRegistration.read_from_file_stocker()
        
        for i in range(0,len(self.stockerList)):
            stocker=self.stockerList[i]
            print("Name :"+stocker.getStockerName())
            print("ID :"+stocker.getStockerId())
        

    
    
    def addBiller(self):
        billerRegistration = BillerRegistration()
        self.billerList = billerRegistration.read_from_file_biller()
        biller = Biller();
        print("Enter biller ID")
        billerId = input()
        for  billerCheck in self.billerList:
            if(billerCheck.getBillerId()==billerId):
                print("biller Id is already exists\n")
                self.addBiller() 
                return  
        biller.setBillerID(billerId)
        print("Enter biller name")
        billerName = input()
        biller.setBillerName(billerName)
        print("Enter biller password")
        billerPassword = input()
        biller.setBillerPassword(billerPassword)
        print("what is your favorite dish")
        securityQuestion = input()
        biller.setBillerSecurityAnswer(securityQuestion)
    
    
        self.billerList.append(biller)
        print("biller added sucesfully...\n")
        
        with open("biller.txt","wb") as f:
            pickle.dump(self.billerList,f)
    
    def displayBillers(self):
        billerRegistration = BillerRegistration()
        self.billerList = billerRegistration.read_from_file_biller()
        
        for i in range(0,len(self.billerList)):
            biller=self.billerlist[i]
            print("Name :"+biller.getBillerName())
            print("ID :"+biller.getBillerId())
        

    def delete_biller(self):
        billerRegistration = BillerRegistration()
        self.billerList = billerRegistration.read_from_file_biller()
        
        id=input("eneter biller id")
        count=0
          
        for i in range(0,len(self.billerList)):
            biller=self.billerList[i]
            if(biller.getBillerId()==id):
                    count=count+1
                    self.billerList.remove(biller)
                    break;
        with open("biller.txt","wb") as f:
            pickle.dump(self.billerList,f)
        if(count==0):
            print("biller id is  not found\n") 
        else:
            print("biller deleted sucesfully\n")  

