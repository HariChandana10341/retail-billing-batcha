import pickle
import os
from pathlib import Path
'''
Created on Dec 20, 2018

@author: IMVIZAG
'''

class BillerRegistration:

    billerList = []
    def read_from_file_biller(self):
        billerfile=Path("biller.txt")
        if(billerfile.is_file()) :
            with open("biller.txt","rb") as f:
                if(os.path.getsize("biller.txt")!=0):
                    self.billerList=pickle.load(f)
        else :
            open("biller.txt","wb")         
            
        return self.billerList

    def login(self):
        
        temp = False
        self.billerList = self.read_from_file_biller()
        print("\tSTOCK DEPARTMENT\n==============================================================================")
        for i in range(0,3): 
            billerName = input("enter biller name")
            billerPwd = input("enter biller password")
            print("------------------------------------------------------------------------------")
            
            for i in range(0,len(self.billerList)):
                if ((self.billerList[i].getBillerName() == billerName) and (self.billerList[i].getBillerPassword()== billerPwd)):
                    temp = True
                    print("successfully logged in")
                    
                    break
            
            if (temp == False):
                print("you have entered invalid credentials.")
            else:
                break
            
        if (temp == False):
            print("your three attempts are completed.\n Did you forgot your password? ")
            
            choice = input("\n 1. yes \n 2. no")
            
            if (choice == "1"):  
                self.forgotPassword(self.billerList) 
            else: 
                print("please try after sometime")
                
    def forgotPassword(self,billerList):  
        count = 1
        for i in range(0,3):
            temp = False
            print("Enter your employee id:")
            biller_Id = input() 
            
            print("\nPlease answer the security question")
            print("\nWhat is your favourite dish?")
            answer = input() 
           
            for i in range(0,len(billerList)):
                if ((billerList[i].getBillerId() == biller_Id) and (billerList[i].getSecurityQuestion() == (answer))) :
                    print("your password is:" + billerList[i].getBillerPassword()) 
                    temp = True
                    break
                
            if (temp == False) :
                if (count < 3): 
                    print("please try again")
                    count+=1
                else :
                    print("Too many attempts!! please try after sometime")
                