import pickle
import os
from pathlib import Path

class StockerRegistration:
    
    stockerList = []
    
    def read_from_file_stocker(self):
        
        stockerfile=Path("stocker.txt")
        if(stockerfile.is_file()) :
            with open("stocker.txt","rb+") as f:
                if(os.path.getsize("stocker.txt")!=0) :
                    self.stockerList=pickle.load(f)
        else:
            open("stocker.txt","wb")
               
        return self.stockerList  
    def login(self):
        
        temp = False
        self.stockerList = self.read_from_file_stocker()
        print("\tSTOCK DEPARTMENT\n==============================================================================")
        for i in range(0,3): 
            stockerName = input("enter stocker name")
            stockerPwd = input("enter stocker password")
            print("------------------------------------------------------------------------------")
            
            for i in range(0,len(self.stockerList)):
                if ((self.stockerList[i].getStockerName() == stockerName) and (self.stockerList[i].getStockerPassword()== stockerPwd)):
                    temp = True
                    print("successfully logged in")
                    
                    break
            
            if (temp == False):
                print("you have entered invalid credentials.")
            else:
                break
            
        if (temp == False):
            print("your three attempts are completed.\n Did you forgot your password? ")
            
            choice = input("\n 1. yes \n 2. no")
            
            if (choice == "1"):  
                self.forgotPassword(self.stockerList) 
            else: 
                print("please try after sometime")
                
    def forgotPassword(self,stockerList):  
        count = 1
        for i in range(0,3):
            temp = False
            print("Enter your employee id:")
            stocker_Id = input() 
            
            print("\nPlease answer the security question")
            print("\nWhat is your favourite dish?")
            answer = input() 
           
            for i in range(0,len(stockerList)):
                if ((stockerList[i].getStockerId() == stocker_Id) and (stockerList[i].getSecurityQuestion() == (answer))) :
                    print("your password is:" + stockerList[i].getStockerPassword()) 
                    temp = True
                    break
                
            if (temp == False) :
                if (count < 3): 
                    print("please try again")
                    count+=1
                else :
                    print("Too many attempts!! please try after sometime")
         