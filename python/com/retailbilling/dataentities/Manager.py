'''
Created on Dec 19, 2018
this class is to create manager object which contains manager entities

@author: Batch A
'''

class Manager:
    
    def getManagerId(self): 
        return self.managerId;

    def setManagerId(self,managerId):
        self.managerId = managerId;
        
    def getManagerName(self): 
        return self.managerName;

    def setManagerName(self,managerName):
        self.managerName = managerName;
        
    def getManagerPassword(self): 
        return self.managerPassword;

    def setManagerPassword(self,managerPassword):
        self.managerPassword = managerPassword;
    
    
    def getSecurityQuestion(self): 
        return self.securityQuestion;

    def setSecurityQuestion(self,securityQuestion):
        self.securityQuestion = securityQuestion;
        
    def display(self):
        print(self.managerId,self.managerName,self.managerPassword,self.securityQuestion)

        