'''
Created on Dec 19, 2018

@author: IMVIZAG
'''

class Biller:

    def getBillerId(self):
        return self.billerId


    def getBillerName(self):
        return self.billerName


    def getBillerPassword(self):
        return self.billerPassword


    def getSecurityQuestion(self):
        return self.securityQuestion


    def setBillerId(self, billerId):
        self.billerId = billerId


    def setBillerName(self, billerName):
        self.billerName = billerName


    def setBillerPassword(self, billerPassword):
        self.billerPassword = billerPassword


    def setSecurityQuestion(self, securityQuestion):
        self.securityQuestion =securityQuestion


    def display(self):
        print(self.billerId,self.billerName,self.billerPassword,self.securityQuestion)
    
   

    