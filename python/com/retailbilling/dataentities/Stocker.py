'''
Created on Dec 19, 2018

@author: batch A
'''

class Stocker:

    def getStockerId(self):
        return self.stockerId


    def getStockerName(self):
        return self.stockerName


    def getStockerPassword(self):
        return self.stockerPassword


    def getSecurityQuestion(self):
        return self.securityQuestion


    def setStockerId(self, stockerId):
        self.stockerId = stockerId


    def setStockerName(self, stockerName):
        self.stockerName = stockerName


    def setStockerPassword(self, stockerPassword):
        self.stockerPassword = stockerPassword


    def setSecurityQuestion(self, securityQuestion):
        self.securityQuestion =securityQuestion


    def display(self):
        print(self.stockerId,self.stockerName,self.stockerPassword,self.securityQuestion)
    
   

    